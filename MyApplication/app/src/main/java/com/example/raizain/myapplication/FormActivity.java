package com.example.raizain.myapplication;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FormActivity extends AppCompatActivity {

    Spinner spinner;
    ArrayAdapter<String> adapter;
    Button btn;
    EditText length, shoulder, nick, chest, waist,tlength,twaist;
    CheckBox sleves;
    String[] detail={
            "Shirt","Fork","Kurta"
    };

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_form);

        length = (EditText)findViewById(R.id.editText);
        shoulder = (EditText)findViewById(R.id.editText2);
        nick = (EditText)findViewById(R.id.editText3);
        chest = (EditText)findViewById(R.id.editText4);
        waist = (EditText)findViewById(R.id.editText5);
        twaist = (EditText)findViewById(R.id.editText6);
        tlength = (EditText)findViewById(R.id.editText7);
        sleves = (CheckBox) findViewById(R.id.checkBox);
        spinner = (Spinner) findViewById(R.id.spinner);
        btn = (Button)findViewById(R.id.button2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("length", length.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("shoulder", shoulder.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("nick", nick.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("chest", chest.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("waist", waist.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("twaist", twaist.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tlength", tlength.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("sleves", sleves.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("spinner", spinner.getSelectedItem().toString()));

                InputStream is = null;
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost("http://10.0.2.2/tutorial.php");
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    String msg = "Data Entered Successfully";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    Log.e("ClientProtocol", "Log-tag");
                    e.printStackTrace();
                }
                Intent intent = new Intent(getBaseContext(),Form2Activity.class);
                startActivity(intent);
            }
        });

      spinner = (Spinner) findViewById(R.id.spinner);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,detail);
        spinner.setAdapter(adapter);
    }
}
