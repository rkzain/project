package com.example.abrar.aep;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;


public class InstructionActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String UPLOAD_URL = "http://192.168.8.107/Android/upload.php";
    public static final String UPLOAD_KEY = "image";
    public static final String TAG = "MY MESSAGE";


    EditText txtpgname, txtownername, txtscope, txtpgdesc;

    public InstructionActivity() {
        txtownername=null;
        txtpgdesc=null;
        txtscope=null;
        txtpgname=null;

    }


    private int PICK_IMAGE_REQUEST = 1;

    private Button buttonChoose;
    private Button btnstart;
    private Button btncapture;
//    private Button buttonView;


    private ImageView imageView;

    private Bitmap bitmap;

    private Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);

        buttonChoose = (Button) findViewById(R.id.buttonChoose);
        btnstart = (Button) findViewById(R.id.btnstart);
        btncapture = (Button) findViewById(R.id.start);
//        buttonView = (Button) findViewById(R.id.buttonViewImage);

        txtpgname = (EditText) findViewById(R.id.txtpgname);
        txtownername = (EditText) findViewById(R.id.txtownername);
        txtpgdesc = (EditText) findViewById(R.id.txtpgdesc);
        txtscope = (EditText) findViewById(R.id.txtscope);



        imageView = (ImageView) findViewById(R.id.imageView);

        buttonChoose.setOnClickListener(this);
        btnstart.setOnClickListener(this);
        btncapture.setOnClickListener(this);

    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void uploadImage(){
        class UploadImage extends AsyncTask<Bitmap,Void,String> {

            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(InstructionActivity.this, "Uploading Image", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Bitmap... params) {
/*                Bitmap bitmap = params[0];
                String uploadImage = getStringImage(bitmap);

                HashMap<String,String> data = new HashMap<>();
                data.put(UPLOAD_KEY, uploadImage);

                String result = rh.sendPostRequest(UPLOAD_URL,data);

                return result;
*/
                Bitmap bitmap = params[0];
                String uploadImage = getStringImage(bitmap);
                String login_url="http://192.168.137.102/Android/upload.php";

                    try {
                        URL url=new URL(login_url);
                        HttpURLConnection connect=(HttpURLConnection)url.openConnection();
                        connect.setRequestMethod("POST");
                        connect.setDoOutput(true);
                        connect.setDoInput(true);
                        OutputStream output= connect.getOutputStream();
                        BufferedWriter bfwrite= new BufferedWriter(new OutputStreamWriter(output, "UTF-8"));
                        String post_data= URLEncoder.encode("image","UTF-8")+"="+URLEncoder.encode(uploadImage,"UTF-8");
                        bfwrite.write(post_data);
                        bfwrite.flush();
                        bfwrite.close();
                        output.close();

                        InputStream input=connect.getInputStream();
                        BufferedReader bfread= new BufferedReader(new InputStreamReader(input,"iso-8859-1"));

                        String result="";
                        String line="";
                        while((line=bfread.readLine())!=null){
                            result += line;
                        }
                        bfread.close();;
                        input.close();;
                        connect.disconnect();

                        return  result;
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                return null;

            }
        }

        UploadImage ui = new UploadImage();
        ui.execute(bitmap);
    }


    public void Checking(){
        String scope=txtscope.getText().toString();
        String pgname=txtpgname.getText().toString();
        String pgdesc=txtpgdesc.getText().toString();
        String ownername=txtownername.getText().toString();
        String type="login";

        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");


        ProfileProcess prox= new ProfileProcess(this);
        prox.execute(type,pgname,pgdesc,ownername,scope,email12);

    }


    @Override
    public void onClick(View v) {
        if (v == buttonChoose) {
            showFileChooser();

        }
        if(v == btnstart){
  //          uploadImage();
            Checking();
            Intent intent = getIntent();
            String email12 = intent.getStringExtra("email");

            Intent i=new Intent(InstructionActivity.this, HomeMain.class);
            i.putExtra("email", email12.toString());
            startActivity(i);

        }
        if(v == btncapture){
            Intent u=new Intent(InstructionActivity.this, UploadActivity.class);
            startActivity(u);
        }
    }


    public class ProfileProcess extends AsyncTask<String, Void, String> {
        Context cntx;
        AlertDialog dialog, dialog1;

        public ProfileProcess(Context ctx) {
            cntx=ctx;
        }

        protected String doInBackground(String... params) {
            String type = params[0];
            String login_url="http://192.168.8.107/Android/share.php";

            if(type.equals("login")){
                try {
                    String pgname= params[1];
                    String pgdesc=params[2];
                    String ownernamw=params[3];
                    String scope=params[4];
                    String email123=params[5];


                    URL url=new URL(login_url);
                    HttpURLConnection connect=(HttpURLConnection)url.openConnection();
                    connect.setRequestMethod("POST");
                    connect.setDoOutput(true);
                    connect.setDoInput(true);
                    OutputStream output= connect.getOutputStream();
                    BufferedWriter bfwrite= new BufferedWriter(new OutputStreamWriter(output, "UTF-8"));
                    String post_data= URLEncoder.encode("email","UTF-8")+"="+URLEncoder.encode(email123,"UTF-8")
                            +"&"+URLEncoder.encode("pagename", "UTF-8")+"="+URLEncoder.encode(pgname,"UTF-8")
                            +"&"+URLEncoder.encode("pagedesc","UTF-8")+"="+URLEncoder.encode(pgdesc,"UTF-8")+"&"
                            +URLEncoder.encode("ownername","UTF-8")+"="+URLEncoder.encode(ownernamw,"UTF-8")+
                            "&"+URLEncoder.encode("scope","UTF-8")+"="+URLEncoder.encode(scope,"UTF-8");
                    bfwrite.write(post_data);
                    bfwrite.flush();
                    bfwrite.close();
                    output.close();

                    InputStream input=connect.getInputStream();
                    BufferedReader bfread= new BufferedReader(new InputStreamReader(input,"iso-8859-1"));

                    String result="";
                    String line="";
                    while((line=bfread.readLine())!=null){
                        result += line;
                    }
                    bfread.close();;
                    input.close();;
                    connect.disconnect();

                    return  result;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            return null;

        }
        @Override
        protected void onPreExecute(){
            dialog = new AlertDialog.Builder(cntx).create();
            dialog.setTitle("Success");
            dialog1 = new AlertDialog.Builder(cntx).create();
            dialog1.setTitle("not success");
        }

        @Override
        protected void onPostExecute(String result){
            if(result.equals("success")) {
                dialog.setMessage(result);
                dialog.show();
    //            Intent u=new Intent(InstructionActivity.this, HomeActivity.class);
     //           startActivity(u);
                Intent intent = getIntent();
                String email12 = intent.getStringExtra("email");

                Intent i=new Intent(InstructionActivity.this, HomeMain.class);
                i.putExtra("email", email12.toString());
                startActivity(i);

            }
            else{
                dialog1.setMessage(result);
                dialog1.show();

            }
        }

    }


}
