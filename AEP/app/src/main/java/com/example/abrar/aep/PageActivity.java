package com.example.abrar.aep;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class PageActivity extends ActionBarActivity {
    String myJSON;


    private static final String TAG_RESULTS="result";
    private static final String Page_Name = "pagename";
    private static final String Page_Desc ="pagedesc";


    private static final String Page_Owner="pageowner";
    private static final String Page_Scope="pagescope";



    JSONArray peoples = null;

    ArrayList<HashMap<String, String>> personList;

    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page);

        list = (ListView) findViewById(R.id.listView1);
        personList = new ArrayList<HashMap<String,String>>();
        getData();
    }
    public  void login(View view){
        Intent i=new Intent(PageActivity.this, SigninActivity.class);
        //       i.putExtra("email", email12.toString());
        startActivity(i);

    }

    public void home(View view){
        Intent i=new Intent(PageActivity.this, WSHomeActivity.class);
        //       i.putExtra("email", email12.toString());
        startActivity(i);
    }

    protected void showList(){
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            peoples = jsonObj.getJSONArray(TAG_RESULTS);

            for(int i=0;i<peoples.length();i++){
                JSONObject c = peoples.getJSONObject(i);
                String name = c.getString(Page_Name);
                String desc = c.getString(Page_Desc);
                String owner = c.getString(Page_Owner);
                String scope = c.getString(Page_Scope);


                HashMap<String,String> persons = new HashMap<String,String>();

                persons.put(Page_Name,name);
                persons.put(Page_Desc,desc);
                persons.put(Page_Owner,owner);
                persons.put(Page_Scope,scope);

                personList.add(persons);
            }

            ListAdapter adapter = new SimpleAdapter(
                    PageActivity.this, personList, R.layout.list_item1,
                    new String[]{Page_Name,Page_Desc,Page_Owner,Page_Scope},
                    new int[]{R.id.pagename, R.id.pagedesc,R.id.ownername,R.id.pagescope}
            );

            list.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public void getData(){
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
                HttpPost httppost = new HttpPost("http://192.168.8.107/Android/gettingpage.php");

                // Depends on your web service
                httppost.setHeader("Content-type", "application/json");

                InputStream inputStream = null;
                String result = null;
                try {
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();
                    // json is UTF-8 by default
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();

                    String line = null;
                    while ((line = reader.readLine()) != null)
                    {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (Exception e) {
                    // Oops
                }
                finally {
                    try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result){
                myJSON=result;
                showList();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





}
