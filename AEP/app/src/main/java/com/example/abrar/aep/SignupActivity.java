package com.example.abrar.aep;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.app.Activity;
import android.view.View.OnClickListener;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


public class SignupActivity extends AppCompatActivity {

    EditText etemail, etpassword, etrepassword;
    Button btnsignup;

    public SignupActivity() {
        btnsignup = null;
        etemail = null;
        etpassword = null;
        etrepassword = null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        etemail = (EditText) findViewById(R.id.txtemail);
        etpassword = (EditText) findViewById(R.id.txtpassword);
        etrepassword = (EditText) findViewById(R.id.repassword);
    }

    public void Check(View view){
        String emailid=etemail.getText().toString();
        String password=etpassword.getText().toString();
        String repassword=etrepassword.getText().toString();
        String type="login";

        Signupprocess pro= new Signupprocess(this);
        pro.execute(type, emailid, password, repassword);



    }
    public void Convert(View view){
        Intent s=new Intent(SignupActivity.this, SigninActivity.class);
        startActivity(s);
    }
    public class Signupprocess extends AsyncTask<String, Void, String > {
        Context cntx;
        AlertDialog dialog, dialog1;


        public Signupprocess(Context ctx) {
            cntx=ctx;
        }

        @Override
        protected String doInBackground(String... params) {
            String type = params[0];
            String login_url="http://192.168.8.107/Android/signup.php";

            if(type.equals("login")){
                try {
                    String emailid1= params[1];
                    String password1=params[2];
                    String repassword=params[3];

                    URL url=new URL(login_url);
                    HttpURLConnection connect=(HttpURLConnection)url.openConnection();
                    connect.setRequestMethod("POST");
                    connect.setDoOutput(true);
                    connect.setDoInput(true);
                    OutputStream output= connect.getOutputStream();
                    BufferedWriter bfwrite= new BufferedWriter(new OutputStreamWriter(output, "UTF-8"));
                    String post_data= URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(emailid1,"UTF-8")+"&"+
                            URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(password1,"UTF-8")+"&"
                            +URLEncoder.encode("repassword","UTF-8")+"="+URLEncoder.encode(repassword,"UTF-8");
                    bfwrite.write(post_data);
                    bfwrite.flush();
                    bfwrite.close();
                    output.close();

                    InputStream input=connect.getInputStream();
                    BufferedReader bfread= new BufferedReader(new InputStreamReader(input,"iso-8859-1"));

                    String result="";
                    String line="";
                    while((line=bfread.readLine())!=null){
                        result += line;
                    }
                    bfread.close();;
                    input.close();;
                    connect.disconnect();

                    return  result;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            return null;

        }
        @Override
        protected void onPreExecute(){
            dialog = new AlertDialog.Builder(cntx).create();
            dialog.setTitle("Lognin Success");
            dialog1 = new AlertDialog.Builder(cntx).create();
            dialog1.setTitle("not success");
        }

        @Override
        protected void onPostExecute(String result){
            if(result.equals("success")) {
                dialog.setMessage(result);
                dialog.show();
                Intent u=new Intent(SignupActivity.this, SigninActivity.class);
                startActivity(u);

            }
            else{
                dialog1.setMessage(result);
                dialog1.show();

            }
        }

    }

}

/*
    @Override
    public void onClick(View v) {
        InsertData task1 = new InsertData();
        task1.execute(new String[]{"http://127.0.0.1/Android/signup.php"});
    }


    private class InsertData extends AsyncTask<String, Void, String> {

        private ProgressDialog dialog = new ProgressDialog(SignupActivity.this);
        private String error;

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Sending Data...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            for (String url : params) {

                    ArrayList<NameValuePair> pair = new ArrayList<NameValuePair>();
                //noinspection ResourceType
                pair.add(new BasicNameValuePair("pagename", etpgname.getText().toString()));
                //noinspection ResourceType
                pair.add(new BasicNameValuePair("email", etemail.getText().toString()));
                //noinspection ResourceType
                pair.add(new BasicNameValuePair("password", etpassword.getText().toString()));


                HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(url);
                try {
                    post.setEntity(new UrlEncodedFormEntity(pair));
                    HttpResponse responce = client.execute(post);
                } catch (IOException e) {
                    error = "Error" + e.getMessage();
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if (s.equals("success")) {
                Toast.makeText(SignupActivity.this,"no success", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(SignupActivity.this, "Success SignUp", Toast.LENGTH_LONG).show();
            }

        }

    }

}
*/