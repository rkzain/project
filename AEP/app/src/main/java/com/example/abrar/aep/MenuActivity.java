package com.example.abrar.aep;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MenuActivity extends AppCompatActivity{


    String editTextId;
        private Button buttonGet;
        private TextView textViewResult;

        private ProgressDialog loading;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_menu);

            Intent intent = getIntent();
            editTextId = intent.getStringExtra("email");

            textViewResult = (TextView) findViewById(R.id.profilename);

            getData();

        }

        private void getData() {
            String id = editTextId.toString().trim();
            if (id.equals("")) {
                Toast.makeText(this, "Please enter an id", Toast.LENGTH_LONG).show();
                return;
            }
            loading = ProgressDialog.show(this,"Please wait...","Fetching...",false,false);

            String url = Config.DATA_URL+editTextId.toString().trim();

            StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    loading.dismiss();
                    showJSON(response);
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MenuActivity.this,error.getMessage().toString(),Toast.LENGTH_LONG).show();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }

        private void showJSON(String response){
            String name="";
            String address="";
            String vc = "";
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray(Config.JSON_ARRAY);
                JSONObject collegeData = result.getJSONObject(0);
                name = collegeData.getString(Config.KEY_NAME);
                address = collegeData.getString(Config.KEY_ADDRESS);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            textViewResult.setText("Owner Name:\t" + name + "\nPage Name:\t" + address);
        }



















    public void upload(View view){
        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");

        Intent i=new Intent(MenuActivity.this, UploadEvent.class);
        i.putExtra("email", email12.toString());
        startActivity(i);
    }
    public void home(View view){
        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");

        Intent i=new Intent(MenuActivity.this, HomeMain.class);
        i.putExtra("email", email12.toString());
        startActivity(i);
    }
    public void login(View view){
        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");

        Intent i=new Intent(MenuActivity.this, SigninActivity.class);
//        i.putExtra("email", email12.toString());
        startActivity(i);
    }
    public void profile(View view){
        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");

        Intent i=new Intent(MenuActivity.this, ProfileSettingActivity.class);
        i.putExtra("email", email12.toString());
        startActivity(i);
    }
    public void page(View view){
        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");

        Intent i=new Intent(MenuActivity.this, PagemenuActivity.class);
        i.putExtra("email", email12.toString());
        startActivity(i);
    }
    public void setting(View view){
        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");

        Intent i=new Intent(MenuActivity.this, SettingActivity.class);
        i.putExtra("email", email12.toString());
        startActivity(i);
    }
    public void feedback(View view){
        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");

        Intent i=new Intent(MenuActivity.this, FeedbackActivity.class);
        i.putExtra("email", email12.toString());
        startActivity(i);
    }



}




