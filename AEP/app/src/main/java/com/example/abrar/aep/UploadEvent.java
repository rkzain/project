package com.example.abrar.aep;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class UploadEvent extends AppCompatActivity {
    EditText Event_name, Event_singer,Event_ticket,Event_desc,Event_location,Event_day,Event_contect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_event);
        Event_name = (EditText) findViewById(R.id.eventname);
        Event_singer = (EditText) findViewById(R.id.singername);
        Event_ticket = (EditText) findViewById(R.id.ticketprice);
        Event_desc = (EditText) findViewById(R.id.eventdesc);
        Event_location = (EditText) findViewById(R.id.location);
        Event_day = (EditText) findViewById(R.id.day);
        Event_contect = (EditText) findViewById(R.id.contect);


    }

    public void menu(View view){
        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");

        Intent i=new Intent(UploadEvent.this, MenuActivity.class);
        i.putExtra("email", email12.toString());
        startActivity(i);

    }

    public void home(View view){
        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");

        Intent i=new Intent(UploadEvent.this, HomeMain.class);
        i.putExtra("email", email12.toString());
        startActivity(i);

    }
    public void login(View view){
        Intent i=new Intent(UploadEvent.this, SigninActivity.class);
        //       i.putExtra("email", email12.toString());
        startActivity(i);

    }

    public void Checking(View view) {
        String  eventname= Event_name.getText().toString();
        String  eventsinger= Event_singer.getText().toString();
        String  eventticket= Event_ticket.getText().toString();
        String  eventdesc= Event_desc.getText().toString();
        String  eventlocation= Event_location.getText().toString();
        String  day= Event_day.getText().toString();
        String  contect= Event_contect.getText().toString();

        Intent intent = getIntent();
        String email12 = intent.getStringExtra("email");

        String type = "login";
        Uploadprocess task = new Uploadprocess(this);
        task.execute(type, eventname, eventsinger, eventticket, eventdesc, eventlocation,email12,day,contect);
    }

    public class Uploadprocess extends AsyncTask<String, Void, String> {
        Context cntx;
        AlertDialog dialog, dialog1;


        public Uploadprocess(Context ctx) {
            cntx = ctx;
        }

        @Override
        protected String doInBackground(String... params) {
            String type = params[0];
            String login_url = "http://192.168.8.107/Android/uploadevent.php";

            if (type.equals("login")) {
                try {
                    String name = params[1];
                    String singer = params[2];
                    String ticket = params[3];
                    String desc = params[4];
                    String location = params[5];
                    String email=params[6];
                    String day = params[7];
                    String contect=params[8];


                    URL url = new URL(login_url);
                    HttpURLConnection connect = (HttpURLConnection) url.openConnection();
                    connect.setRequestMethod("POST");
                    connect.setDoOutput(true);
                    connect.setDoInput(true);
                    OutputStream output = connect.getOutputStream();
                    BufferedWriter bfwrite = new BufferedWriter(new OutputStreamWriter(output, "UTF-8"));
                    String post_data = URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8") + "&"
                            + URLEncoder.encode("singer", "UTF-8") + "=" + URLEncoder.encode(singer, "UTF-8")+ "&"
                            + URLEncoder.encode("ticket", "UTF-8") + "=" + URLEncoder.encode(ticket, "UTF-8")+ "&"
                            + URLEncoder.encode("desc", "UTF-8") + "=" + URLEncoder.encode(desc, "UTF-8")+ "&"
                            + URLEncoder.encode("address", "UTF-8") + "=" + URLEncoder.encode(location, "UTF-8")+ "&"
                            + URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8")+ "&"
                            + URLEncoder.encode("day", "UTF-8") + "=" + URLEncoder.encode(day, "UTF-8")+ "&"
                            + URLEncoder.encode("contect", "UTF-8") + "=" + URLEncoder.encode(contect, "UTF-8");
                    bfwrite.write(post_data);
                    bfwrite.flush();
                    bfwrite.close();
                    output.close();

                    InputStream input = connect.getInputStream();
                    BufferedReader bfread = new BufferedReader(new InputStreamReader(input, "iso-8859-1"));

                    String result = "";
                    String line = "";
                    while ((line = bfread.readLine()) != null) {
                        result += line;
                    }
                    bfread.close();
                    ;
                    input.close();
                    ;
                    connect.disconnect();

                    return result;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            return null;

        }

        @Override
        protected void onPreExecute() {
            dialog = new AlertDialog.Builder(cntx).create();
            dialog.setTitle("Lognin Success");
            dialog1 = new AlertDialog.Builder(cntx).create();
            dialog1.setTitle("not success");
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals("success")) {

                Intent intent = getIntent();
                String email12 = intent.getStringExtra("email");

                Intent i=new Intent(UploadEvent.this, HomeMain.class);
                i.putExtra("email", email12.toString());
                startActivity(i);


            } else if(result.equals("succes")) {

                Intent i=new Intent(UploadEvent.this, HomeMain.class);
//                i.putExtra("email", etemail.getText().toString());
                startActivity(i);

            }
            else{
                dialog1.setMessage(result);
                dialog1.show();
            }
        }
    }



}
