package com.example.abrar.aep;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kosalgeek.asynctask.AsyncResponse;
import com.kosalgeek.asynctask.PostResponseAsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;


public class SigninActivity extends AppCompatActivity {
    EditText etemail, etpassword;
    Button btnsignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        etemail = (EditText) findViewById(R.id.emailid);
        etpassword = (EditText) findViewById(R.id.password);


    }


    public void Checking(View view) {
        String email = etemail.getText().toString();
        String password = etpassword.getText().toString();
        String type = "login";
        Loginprocess task = new Loginprocess(this);
        task.execute(type, email, password);
    }

    public void Convert(View view) {
        Intent s = new Intent(SigninActivity.this, SignupActivity.class);
        startActivity(s);
    }


    public class Loginprocess extends AsyncTask<String, Void, String> {
        Context cntx;
        AlertDialog dialog, dialog1;


        public Loginprocess(Context ctx) {
            cntx = ctx;
        }

        @Override
        protected String doInBackground(String... params) {
            String type = params[0];
            String login_url = "http://192.168.8.107/Android/connect.php";

            if (type.equals("login")) {
                try {
                    String email = params[1];
                    String password = params[2];
                    URL url = new URL(login_url);
                    HttpURLConnection connect = (HttpURLConnection) url.openConnection();
                    connect.setRequestMethod("POST");
                    connect.setDoOutput(true);
                    connect.setDoInput(true);
                    OutputStream output = connect.getOutputStream();
                    BufferedWriter bfwrite = new BufferedWriter(new OutputStreamWriter(output, "UTF-8"));
                    String post_data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8") + "&"
                            + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");
                    bfwrite.write(post_data);
                    bfwrite.flush();
                    bfwrite.close();
                    output.close();

                    InputStream input = connect.getInputStream();
                    BufferedReader bfread = new BufferedReader(new InputStreamReader(input, "iso-8859-1"));

                    String result = "";
                    String line = "";
                    while ((line = bfread.readLine()) != null) {
                        result += line;
                    }
                    bfread.close();
                    ;
                    input.close();
                    ;
                    connect.disconnect();

                    return result;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            return null;

        }

        @Override
        protected void onPreExecute() {
            dialog = new AlertDialog.Builder(cntx).create();
            dialog.setTitle("Lognin Success");
            dialog1 = new AlertDialog.Builder(cntx).create();
            dialog1.setTitle("not success");
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals("success")) {

                Intent i=new Intent(SigninActivity.this, InstructionActivity.class);
                i.putExtra("email", etemail.getText().toString());
                startActivity(i);


            } else if(result.equals("succes")) {

                Intent i=new Intent(SigninActivity.this, HomeMain.class);
                i.putExtra("email", etemail.getText().toString());
                startActivity(i);

            }
            else{
                dialog1.setMessage(result);
                dialog1.show();
            }

        }
    }
}


/*
    EditText etemail, etpassword;
    Button btnsignin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        etemail=(EditText) findViewById(R.id.emailid);
        etpassword=(EditText) findViewById(R.id.password);

        btnsignin=(Button) findViewById(R.id.btnSignin);
        btnsignin.setOnClickListener(this);
    }

    @Override
    public void processFinish(String s) {
 //       if(s.equals("success")) {
//            Toast.makeText(this, s ,Toast.LENGTH_LONG). show();
//            Intent next=new Intent(this, HomeActivity.class);
//            startActivity(next);
/*        }
        else if(s.equals("true")){
            Toast.makeText(this, "true", Toast.LENGTH_LONG).show();
        }
        else if(s.equals("failed")){
            Toast.makeText(this, "failed", Toast.LENGTH_LONG).show();
        }
        else if(s.equals("L")) {
            Toast.makeText(this, "L", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this, "L", Toast.LENGTH_LONG).show();
        }
        }
 */
/*
    @Override
    public void onClick(View v) {
//        HashMap postData =  new HashMap();
  //       postData.put("email", etemail.getText().toString());
    //    postData.put("password", etpassword.getText().toString());
        String username= etemail.getText().toString();
        String password= etpassword.getText().toString();



        PostResponseAsyncTask task=new PostResponseAsyncTask(this, new AsyncResponse() {
            @Override
            public void processFinish(String s) {
                Toast.makeText(SigninActivity.this, s ,Toast.LENGTH_LONG). show();
            }
        });
//        PostResponseAsyncTask task=new PostResponseAsyncTask(this,postData);
        task.execute("https://192.168.137.101/Android/connect.php");
    }
}
*/

