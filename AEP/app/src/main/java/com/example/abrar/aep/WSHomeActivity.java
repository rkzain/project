package com.example.abrar.aep;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class WSHomeActivity extends AppCompatActivity {

    String myJSON;


    private static final String TAG_RESULTS="result";
    private static final String TAG_NAME = "pagename";
    private static final String TAG_ADD ="address";


    private static final String Event_name="eventname";
    private static final String Event_date="eventdate";
    private static final String Event_desc="eventdesc";
    private static final String Event_singer="eventsinger";
    private static final String Event_ticket="ticket";
    private static final String Event_day="eventday";
    private static final String Event_contect="contect";



    JSONArray peoples = null;

    ArrayList<HashMap<String, String>> personList;

    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wshome);

        list = (ListView) findViewById(R.id.listView);
        personList = new ArrayList<HashMap<String,String>>();
        getData();
    }
    public  void login(View view){
        Intent i=new Intent(WSHomeActivity.this, SigninActivity.class);
        //       i.putExtra("email", email12.toString());
        startActivity(i);

    }

    public void page(View view){

        Intent i=new Intent(WSHomeActivity.this, PageActivity.class);
        startActivity(i);

//        Intent i=new Intent(HomeMain.this, UploadEvent.class);
        //      i.putExtra("email", email12.toString());
        //    startActivity(i);

    }

    protected void showList(){
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            peoples = jsonObj.getJSONArray(TAG_RESULTS);

            for(int i=0;i<peoples.length();i++){
                JSONObject c = peoples.getJSONObject(i);
                String name = c.getString(TAG_NAME);
                String address = c.getString(TAG_ADD);
                String event_name = c.getString(Event_name);
                String event_desc = c.getString(Event_desc);
                String date = c.getString(Event_date);
                String singer = c.getString(Event_singer);
                String ticket= c.getString(Event_ticket);
                String day = c.getString(Event_day);
                String contect= c.getString(Event_contect);


                HashMap<String,String> persons = new HashMap<String,String>();

                persons.put(TAG_NAME,name);
                persons.put(TAG_ADD,address);
                persons.put(Event_name,event_name);
                persons.put(Event_desc,event_desc);
                persons.put(Event_date,date);
                persons.put(Event_singer,singer);
                persons.put(Event_ticket,ticket);
                persons.put(Event_day,day);
                persons.put(Event_contect,contect);

                personList.add(persons);
            }

            ListAdapter adapter = new SimpleAdapter(
                    WSHomeActivity.this, personList, R.layout.list_item,
                    new String[]{TAG_NAME,TAG_ADD,Event_name,Event_desc,Event_date,Event_singer,Event_ticket,Event_day,Event_contect},
                    new int[]{R.id.name, R.id.address,R.id.eventname,R.id.eventdesc,R.id.eventdate,R.id.eventsinger,R.id.ticket,R.id.eventday,R.id.contect}
            );

            list.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public void getData(){
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
                HttpPost httppost = new HttpPost("http://192.168.8.107/Android/getting.php");

                // Depends on your web service
                httppost.setHeader("Content-type", "application/json");

                InputStream inputStream = null;
                String result = null;
                try {
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();
                    // json is UTF-8 by default
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();

                    String line = null;
                    while ((line = reader.readLine()) != null)
                    {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (Exception e) {
                    // Oops
                }
                finally {
                    try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result){
                myJSON=result;
                showList();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
